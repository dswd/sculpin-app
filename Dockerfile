FROM cloudron/base:0.10.0
MAINTAINER Dennis Schwerdel <schwerdel@googlemail.com>

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update \
 && apt-get install -y composer nginx proftpd proftpd-mod-ldap \
 && rm -rf /var/cache/apt /var/lib/apt/lists

# Nginx
RUN mkdir /run/nginx \
 && rm /var/log/nginx/*.log \
 && ln -s /dev/stderr /var/log/nginx/error.log \
 && ln -s /dev/stdout /var/log/nginx/access.log \
 && rmdir /var/lib/nginx && ln -s /tmp /var/lib/nginx \
 && chown -R cloudron:cloudron /run/nginx

# Proftpd
ADD proftpd.conf /app/code/proftpd.conf
RUN rm -rf /var/log/proftpd \
 && ln -s /run/proftpd /var/log/proftpd

# Supervisor
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

# Sculpin
ADD composer.json .
RUN composer install \
 && rm -rf /root/.composer \
 && ln -s /run/composer /root/.composer
ADD sculpin /usr/local/bin/sculpin

# Config & initial site
ADD template template

ADD nginx.conf .
ADD start.sh .

WORKDIR /app/data

CMD [ "/app/code/start.sh" ]
