# Sculpin Cloudron App

This repository contains the Cloudron app package source for [Sculpin](http://sculpin.io/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=io.sculpin.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id io.sculpin.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd sculpin-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/) and [vdirsyncer](https://github.com/untitaker/vdirsyncer). They are creating a fresh build, install the app on your Cloudron, perform carddav tests, backup, restore and test if the carddav contacts are still ok. Due to limitations of vdirsyncer, the tests only work with valid SSL certificates.

```
cd sculpin-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha test.js
```

