#!/bin/bash

set -eu

mkdir -p /run/proftpd
# SFTP_PORT can be unset to disable SFTP
disable_sftp="false"
if [[ -z "${SFTP_PORT:-}" ]]; then
    echo "SFTP disabled"
    SFTP_PORT=29418 # arbitrary port to keep sshd happy
fi
sed -e "s,##SERVER_NAME,${APP_DOMAIN}," \
    -e "s/##SFTP_PORT/${SFTP_PORT}/" \
    -e "s,##LDAP_URL,${LDAP_URL},g" \
    -e "s/##LDAP_BIND_DN/${LDAP_BIND_DN}/g" \
    -e "s/##LDAP_BIND_PASSWORD/${LDAP_BIND_PASSWORD}/g" \
    -e "s/##LDAP_USERS_BASE_DN/${LDAP_USERS_BASE_DN}/g" \
    -e "s/##LDAP_UID/$(id -u cloudron)/g" \
    -e "s/##LDAP_GID/$(id -g cloudron)/g" \
    /app/code/proftpd.conf > /run/proftpd/proftpd.conf

if [[ ! -f "/app/data/sftpd/ssh_host_ed25519_key" ]]; then
    echo "Generating ssh host keys"
    mkdir -p /app/data/sftpd
    ssh-keygen -qt rsa -N '' -f /app/data/sftpd/ssh_host_rsa_key
    ssh-keygen -qt dsa -N '' -f /app/data/sftpd/ssh_host_dsa_key
    ssh-keygen -qt ecdsa -N '' -f /app/data/sftpd/ssh_host_ecdsa_key
    ssh-keygen -qt ed25519 -N '' -f /app/data/sftpd/ssh_host_ed25519_key
else
    echo "Reusing existing host keys"
fi
chmod 0600 /app/data/sftpd/*_key
chmod 0644 /app/data/sftpd/*.pub


if ! [ -d "/app/data/source" ]; then
    cp -a /app/code/template/* "/app/data/"
fi

chown -R cloudron:cloudron /app/data

mkdir -p /run/composer

if ! [ -d "/run/sculpin" ]; then
    mkdir -p /run/sculpin/app
    ln -s /app/data/config /run/sculpin/app/config
    ln -s /app/data/source /run/sculpin/source
    ln -s /app/code/vendor /run/sculpin/vendor
fi

sculpin install --project-dir=/app/data
echo "Starting supervisord"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Lamp

